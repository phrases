% phrases-extract(1) 1.0.3
% Armaan Bhojwani
% January 2021

# NAME
phrases-extract - Extract Latin famous phrases from Wikipedia

# SYNOPSIS
phrases-extract [[options]]

# DESCRIPTION
**phrases-extract** is a Python script that converts the Wikipedia table of Latin famous phrases into a SQLite3 database for use with the **phrases** program

# OPTIONS
**-h**, **--help**
: Show a help message and exit

**-o** [file], **--output** [file]
: Specify the output file location. Default = ./phrases.db

**-v**, **--version**
: Print version

# EXIT VALUES
**0**
: Success

**2**
: Invalid option

# SEE ALSO
  **phrases(1)**

# BUGS, PATCHES
https://lists.sr.ht/~armaan/public-inbox

# COPYRIGHT
Copyright 2021 Armaan Bhojwani <me@armaanb.net>. MIT License
